
# Create IAM User
resource "aws_iam_user" "iam-user" {
  name = "iam-user"
}

# Create IAM User groups (Admin, Developer, ReadOnly or Monitoring)
resource "aws_iam_group" "admin-group" {
  name = "Admin"
}

resource "aws_iam_group" "developer-group" {
  name = "Developer"
}

resource "aws_iam_group" "readonly-group" {
  name = "userReadonly"
}

# IAM Group membership to manage IAM Users
resource "aws_iam_group_membership" "team" {
  name = "tf-testing-group-membership"

  users = [
    aws_iam_user.iam-user.name,
  ]

  group = aws_iam_group.admin-group.name
#  group = aws_iam_group.developer-group.name
#  group = aws_iam_group.readonly-group.name

}


resource "aws_kms_key" "mykey" {
  description             = "The KMS Key1"
  deletion_window_in_days = 10
  enable_key_rotation = true
}


data "aws_caller_identity" "current"{}

data "aws_iam_policy_document" "key_policy" {
statement {
sid = "allow root user permission"
effect = "Allow"
principals {
type = "AWS"
identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
}
actions = ["kms:*"]
resources = ["*"]
}
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    effect = "Allow"
    actions = [
      "s3:ListAllMyBuckets"
    ]

    resources = [
      "arn:aws:s3:::tfiam"
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "s3:GetObject",
    ]

    resources = [
      "arn:aws:s3:::tfiam/*"
    ]
  }
}

resource "aws_iam_policy" "execrise_policy" {
  name        = var.s3_policy_name
  description = "policy that will be used for s3"
  policy = data.aws_iam_policy_document.s3_policy.json
}
  
resource "aws_iam_group_policy_attachment" "custom_policy" {
  group      = aws_iam_group.admin-group.name
  policy_arn = aws_iam_policy.execrise_policy.arn
}
