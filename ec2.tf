resource "aws_iam_instance_profile" "ec2_profile" {
  name = "var.profile"
  role = "${aws_iam_role.execrise_iam_role.name}"
}

resource "aws_instance" "my-test-instance" {
  ami             = var.ami_type
  instance_type   = var.instance_type
  iam_instance_profile = "${aws_iam_instance_profile.ec2_profile.name}"
  key_name = var.key_pair
  tags = {
    Name = "test-instance"
  }
}


resource "tls_private_key" "goodkey" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "ec2_instance_key" {
  key_name   = var.key_pair
  public_key = tls_private_key.goodkey.public_key_openssh

# The code below creates myKey to AWS and myKey.pem to your computerand the created myKey and myKey.pem have the same private keys. 
  provisioner "local-exec" { # Create "myKey.pem" to your computer!!
    command = "echo '${tls_private_key.goodkey.private_key_pem}' > ./myKey.pem"
}
}

