data "aws_iam_policy_document" "ec2_role_policy" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}



resource "aws_iam_role" "execrise_role" {
  name = "Ec2-Role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF

  tags = {
    name = "TerraformRole"
  }
}


resource "aws_iam_role" "execrise_iam_role" {
  name = "TerraformRole"

  assume_role_policy = data.aws_iam_policy_document.ec2_role_policy.json

  tags = {
    name = "TerraformRole"
  }
}

resource "aws_iam_role_policy_attachment" "attach" {
  role       = aws_iam_role.execrise_iam_role.name
  policy_arn = aws_iam_policy.execrise_policy.arn
}